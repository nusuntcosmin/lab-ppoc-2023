package com.example.implementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {

    private ConcurrentHashMap<Object, T> hashMap;

    public ConcurrentHashMapBasedRepository() {
        hashMap = new ConcurrentHashMap<>();
    }

    @Override
    public void add(T entity) {
        hashMap.put(entity.hashCode(), entity);
    }

    @Override
    public boolean contains(T entity) {
        return hashMap.containsKey(entity.hashCode());
    }

    @Override
    public void remove(T entity) {
        hashMap.remove(entity.hashCode());
    }

    public void clear() {
        hashMap.clear();
    }

}
