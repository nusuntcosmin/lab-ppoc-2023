package com.example.implementation;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {

    private Set<T> set;

    public TreeSetBasedRepository() {
        set = new TreeSet<>();
    }

    @Override
    public void add(T entity) {
        set.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return set.contains(entity);
    }

    @Override
    public void remove(T entity) {
        set.remove(entity);
    }

    public void clear(){
        set.clear();
    }

    public void addAll(Collection<T> collection){
        set.addAll(collection);
    }
}
