package com.example.implementation;

import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.list.mutable.FastList;

import java.util.Collection;

public class ElicpseMutableListBasedRepository<T> implements InMemoryRepository<T>{

    private MutableList<T> list;

    public ElicpseMutableListBasedRepository() {
        list = new FastList<>();
    }

    @Override
    public void add(T entity) {
        list.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return list.contains(entity);
    }

    @Override
    public void remove(T entity) {
        list.remove(entity);
    }

    public void clear() {
        list.clear();
    }

    public void addAll(Collection<T> collection) {
        list.addAll(collection);
    }
}
