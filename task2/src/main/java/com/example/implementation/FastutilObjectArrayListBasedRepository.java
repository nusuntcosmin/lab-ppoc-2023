package com.example.implementation;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;

import java.util.Collection;

public class FastutilObjectArrayListBasedRepository<T> implements InMemoryRepository<T> {

    private ObjectArrayList<T> list;

    public FastutilObjectArrayListBasedRepository() {
        list = new ObjectArrayList<>();
    }

    @Override
    public void add(T entity) {
        list.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return list.contains(entity);
    }

    @Override
    public void remove(T entity) {
        list.remove(entity);
    }

    public void clear(){
        list.clear();
    }

    public void addAll(Collection<T> collection) {
        list.addAll(collection);
    }
}
