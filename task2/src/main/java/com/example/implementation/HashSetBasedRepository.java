package com.example.implementation;

import com.example.Order;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository<T> implements InMemoryRepository<T> {

    private Set<T> set;

    public HashSetBasedRepository() {
        set = new HashSet<>();
    }

    @Override
    public void add(T entity) {
        set.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return set.contains(entity);
    }

    @Override
    public void remove(T entity) {
        set.remove(entity);
    }

    public void clear(){
        set.clear();
    }

    public void addAll(Collection<T> collection) {
        set.addAll(collection);
    }
}
