package com.example.implementation;

import com.example.Order;

import java.util.ArrayList;
import java.util.Collection;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {

    private ArrayList<T> list;

    public ArrayListBasedRepository() {
        list = new ArrayList<>();
    }

    @Override
    public void add(T entity) {
        list.add(entity);
    }

    @Override
    public boolean contains(T entity) {
        return list.contains(entity);
    }

    public void clear(){
        list.clear();
    }

    public void addAll(Collection<T> collection) {
        list.addAll(collection);
    }
    @Override
    public void remove(T entity) {
        list.remove(entity);
    }
}
