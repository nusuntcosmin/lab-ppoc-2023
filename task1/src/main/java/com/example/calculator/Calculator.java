package com.example.calculator;

import com.example.operation.*;
import com.example.utils.StringUtils;

public class Calculator {

    private Operation getOperation(String operator){
        switch (operator) {
            case "+":
                return new Addition();
            case "-":
                return new Subtraction();
            case "*":
                return new Multiplication();
            case "/":
                return new Division();
            case "max":
                return new Maximum();
            case "min":
                return new Minimum();
            default:
                throw new IllegalArgumentException("Unknown operator: " + operator);
        }
    }

    public double calculateOperation(String inputOperation) throws Exception{
        if (inputOperation.isEmpty())
            throw new Exception("Input cannot be empty");

        String[] operationComponents = inputOperation.split(" ");

        if (operationComponents.length != 3)
            throw new Exception("Invalid operation.");

        double firstOperand;
        String operator;
        double secondOperand;

        if(StringUtils.hasNoDigits(operationComponents[0])) {
            firstOperand = Double.parseDouble(operationComponents[1]);
            operator = operationComponents[0];
            secondOperand = Double.parseDouble(operationComponents[2]);
        } else {
            firstOperand = Double.parseDouble(operationComponents[0]);
            operator = operationComponents[1];
            secondOperand = Double.parseDouble(operationComponents[2]);
        }

        Operation operationToCalculate = getOperation(operator);
        return operationToCalculate.calculateOperation(firstOperand, secondOperand);
    }
}
