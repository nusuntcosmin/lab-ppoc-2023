package com.example.utils;

public class StringUtils {
    public static boolean hasNoDigits(String string) {
        char[] chars = string.toCharArray();
        for (char c : chars) {
            if(!Character.isLetter(c)) {
                return false;
            }
        }

        return true;
    }
}
