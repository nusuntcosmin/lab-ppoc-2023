package com.example;

import com.example.calculator.Calculator;
import org.checkerframework.checker.units.qual.C;

import java.util.Scanner;

public class Main {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String givenInput = scanner.nextLine();
        Calculator calculator = new Calculator();

        try{
            System.out.println("The result of the operation is " + calculator.calculateOperation(givenInput));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
