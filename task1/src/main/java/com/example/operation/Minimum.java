package com.example.operation;

public class Minimum implements Operation{
    @Override
    public double calculateOperation(double x, double y) throws ArithmeticException {
        return Math.min(x,y);
    }
}
