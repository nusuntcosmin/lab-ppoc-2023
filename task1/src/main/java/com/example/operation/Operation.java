package com.example.operation;

public interface Operation {
    double calculateOperation(double x, double y) throws ArithmeticException;
}
