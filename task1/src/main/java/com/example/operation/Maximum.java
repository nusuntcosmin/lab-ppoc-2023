package com.example.operation;

public class Maximum implements Operation{
    @Override
    public double calculateOperation(double x, double y) throws ArithmeticException {
        return Math.max(x,y);
    }
}
