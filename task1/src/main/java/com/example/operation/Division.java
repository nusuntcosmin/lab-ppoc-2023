package com.example.operation;

public class Division implements Operation{
    @Override
    public double calculateOperation(double x, double y) {
        if(y == 0)
            throw new ArithmeticException("Denominator cannot be 0");
        return x / y;
    }
}
