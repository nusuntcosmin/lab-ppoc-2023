package com.example.operation;

public class Subtraction implements Operation{
    @Override
    public double calculateOperation(double x, double y) {
        return x - y;
    }
}
