package calculator;

import com.example.calculator.Calculator;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
public class CalculatorTest {

    private final Calculator testCalculator = new Calculator();

    @Test
    public void testAddition() {
        final String firstInput = "2 + 3";
        final String secondInput = "0 + 0";
        final String thirdInput = "2 + -1";
        final String fourthInput = "+ 1 2";

        try {
            double firstResult = testCalculator.calculateOperation(firstInput);
            double secondResult = testCalculator.calculateOperation(secondInput);
            double thirdResult = testCalculator.calculateOperation(thirdInput);
            assertEquals(5,firstResult);
            assertEquals(0,secondResult);
            assertEquals(1,thirdResult);

            double fourthResult = testCalculator.calculateOperation(fourthInput);
            fail();
        } catch (Exception ex) {
            assertTrue(true);
        };
    }

    @Test
    public void testSubtraction() {
        final String firstInput = "2 - 3";
        final String secondInput = "10 - 9";
        final String thirdInput = "-2 - 3";
        final String fourthInput = "";

        try {
            double firstResult = testCalculator.calculateOperation(firstInput);
            double secondResult = testCalculator.calculateOperation(secondInput);
            double thirdResult = testCalculator.calculateOperation(thirdInput);
            assertEquals(-1,firstResult);
            assertEquals(1,secondResult);
            assertEquals(-5,thirdResult);
            double fourthResult = testCalculator.calculateOperation(fourthInput);
            fail();
        } catch (Exception ex) {
            assertTrue(true);
        };
    }

    @Test
    public void testMultiplication() {
        final String firstInput = "33 * 82";
        final String secondInput = "321321321 * 0";
        final String thirdInput = "-42 * -42";
        final String fourthInput = "-3 * 10";

        try {
            double firstResult = testCalculator.calculateOperation(firstInput);
            double secondResult = testCalculator.calculateOperation(secondInput);
            double thirdResult = testCalculator.calculateOperation(thirdInput);
            double fourthResult = testCalculator.calculateOperation(fourthInput);
            assertEquals(2706,firstResult);
            assertEquals(0,secondResult);
            assertEquals(1764,thirdResult);
            assertEquals(-30,fourthResult);
        } catch (Exception ex) {
            fail();
        };
    }

    @Test
    public void testDivision() {
        final String firstInput = "5 / 2";
        final String secondInput = "27 / 9";
        final String thirdInput = "10 / 3";
        final String fourthInput = "1321321 / 0";

        try {
            double firstResult = testCalculator.calculateOperation(firstInput);
            double secondResult = testCalculator.calculateOperation(secondInput);
            double thirdResult = testCalculator.calculateOperation(thirdInput);
            assertEquals(2.5,firstResult);
            assertEquals(3,secondResult);
            assertEquals(3.3333333333333335,thirdResult);
            double fourthResult = testCalculator.calculateOperation(fourthInput);
            fail();
        } catch (Exception ex) {
            assertTrue(true);
        };
    }

    @Test
    public void testMaximum() {
        final String firstInput = "max 5 2";
        final String secondInput = "max -1 0";
        final String thirdInput = "max 10 2";
        final String fourthInput = "1 max 2";

        try {
            double firstResult = testCalculator.calculateOperation(firstInput);
            double secondResult = testCalculator.calculateOperation(secondInput);
            double thirdResult = testCalculator.calculateOperation(thirdInput);
            double fourthResult = testCalculator.calculateOperation(fourthInput);
            assertEquals(5,firstResult);
            assertEquals(0,secondResult);
            assertEquals(10,thirdResult);
            assertEquals(2,fourthResult);
        } catch (Exception ex) {
            fail();
        };
    }

    @Test
    public void testMinimum() {
        final String firstInput = "min 5 2";
        final String secondInput = "min -1 0";
        final String thirdInput = "min 10 2";
        final String fourthInput = "1 min 2";

        try {
            double firstResult = testCalculator.calculateOperation(firstInput);
            double secondResult = testCalculator.calculateOperation(secondInput);
            double thirdResult = testCalculator.calculateOperation(thirdInput);
            double fourthResult = testCalculator.calculateOperation(fourthInput);
            assertEquals(2, firstResult);
            assertEquals(-1,secondResult);
            assertEquals(2, thirdResult);
            assertEquals(1, fourthResult);
        } catch (Exception ex) {
            fail();
        };
    }

}
